package com.hendisantika.springbootreactoauth2sociallogindemo;

import com.hendisantika.springbootreactoauth2sociallogindemo.config.AppProperties;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(AppProperties.class)
public class SpringBootReactOauth2SocialLoginDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootReactOauth2SocialLoginDemoApplication.class, args);
    }
}
