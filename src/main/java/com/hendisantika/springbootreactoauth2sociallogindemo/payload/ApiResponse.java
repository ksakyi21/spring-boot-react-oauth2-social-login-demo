package com.hendisantika.springbootreactoauth2sociallogindemo.payload;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-react-oauth2-social-login-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-29
 * Time: 06:51
 * To change this template use File | Settings | File Templates.
 */
@Data
@AllArgsConstructor
public class ApiResponse {
    private boolean success;
    private String message;
}
