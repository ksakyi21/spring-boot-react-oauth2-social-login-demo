package com.hendisantika.springbootreactoauth2sociallogindemo.entity;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-react-oauth2-social-login-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-29
 * Time: 06:44
 * To change this template use File | Settings | File Templates.
 */
public enum AuthProvider {
    local,
    facebook,
    google,
    github
}
