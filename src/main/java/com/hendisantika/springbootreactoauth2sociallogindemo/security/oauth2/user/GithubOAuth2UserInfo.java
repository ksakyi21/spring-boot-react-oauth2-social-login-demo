package com.hendisantika.springbootreactoauth2sociallogindemo.security.oauth2.user;

import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * Project : spring-boot-react-oauth2-social-login-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 2018-11-29
 * Time: 07:04
 * To change this template use File | Settings | File Templates.
 */
public class GithubOAuth2UserInfo extends OAuth2UserInfo {

    public GithubOAuth2UserInfo(Map<String, Object> attributes) {
        super(attributes);
    }

    @Override
    public String getId() {
        return ((Integer) attributes.get("id")).toString();
    }

    @Override
    public String getName() {
        return (String) attributes.get("name");
    }

    @Override
    public String getEmail() {
        return (String) attributes.get("email");
    }

    @Override
    public String getImageUrl() {
        return (String) attributes.get("avatar_url");
    }
}